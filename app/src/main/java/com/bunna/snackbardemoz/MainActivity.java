package com.bunna.snackbardemoz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    LinearLayout llMain;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<Receipt>> expandableListDetail;

    public static Receipt back_receipt = null;
    public static Receipt next_receipt = null;

    int groupPosition1, childPosition1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        llMain = (LinearLayout) findViewById(R.id.llMain);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expandableListDetail = ExpandableListDataPump.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);


        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

//                boolean is_back_available = false;
//                boolean is_next_available = false;
//
//                    //is_back_available check
//                    //all group loop
//                    for (int g = groupPosition; g >= 0; g--) {
//
//                        Log.v("demozs", "g " + g);
//
//                        if (g == groupPosition) { //current group
//                            for (int i = childPosition ; i >= 0 ; i--) {
//
//                                if (i==0 ) {
//                                    is_back_available = false;
//                                } else  {
//                                    Receipt receipt = (Receipt) expandableListAdapter.getChild(g,i-1);
//
//                                    if (!receipt.isIs_submitted()) { // has receipt not yet submit
//                                        is_back_available = true;
//                                        break ;
//                                    } else {
//                                        is_back_available = false;
//                                    }
//
//                                }
//
//                            }
//                        } else { //previous group
//
//                            for(int i=0 ; i < expandableListAdapter.getChildrenCount(g); i++) {
//                                Receipt receipt = (Receipt) expandableListAdapter.getChild(g,i);
//                                if (!receipt.isIs_submitted()) { // has receipt not yet submit
//                                    Log.v("demozs", "not yet");
//                                    is_back_available = true;
//                                    break ;
//                                } else {
//                                    Log.v("demozs", "already");
//                                    is_back_available = false;
//                                }
//                            }
//                        }
//
//                        //also break this loop when is_next_available = true
//                        if (is_back_available) break;
//
//                    }
//
//
//                //is_next_available
//                //all group loop
//                for (int g = groupPosition; g < expandableListAdapter.getGroupCount(); g++) {
//
//                    Log.v("demozs", "g " + g);
//
//                    if (g == groupPosition) { //current group
//                        for (int i = childPosition ; i < expandableListAdapter.getChildrenCount(g) ; i++) {
//
//                            if (i==childPosition) {
//                                is_next_available = false;
//                            } else  {
//                                Receipt receipt = (Receipt) expandableListAdapter.getChild(g,i);
//
//                                if (!receipt.isIs_submitted()) { // has receipt not yet submit
//                                    is_next_available = true;
//                                    break ;
//                                } else {
//                                    is_next_available = false;
//                                }
//
//                            }
//
//                        }
//                    } else { //next group
//
//                        for(int i=0 ; i < expandableListAdapter.getChildrenCount(g); i++) {
//                            Receipt receipt = (Receipt) expandableListAdapter.getChild(g,i);
//                            if (!receipt.isIs_submitted()) { // has receipt not yet submit
//                                Log.v("demozs", "not yet");
//                                is_next_available = true;
//                                break ;
//                            } else {
//                                Log.v("demozs", "already");
//                                is_next_available = false;
//                            }
//                        }
//                    }
//
//                    //also break this loop when is_next_available = true
//                    if (is_next_available) break;
//
//                }
//
//                Constant.ButtonSnackBar.IS_BACK_AVAILABLE = is_back_available;
//                Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = is_next_available;
//
//                Toast.makeText(MainActivity.this, "Back : " + is_back_available + " Next : " + is_next_available, Toast.LENGTH_SHORT).show();
                

                /*

                    Three condition
                    1. first group click
                    2. last group click
                    3. other group click

                    in each group has three condition
                    1. first child click
                    2. last child click
                    3. other child click

                 */


                groupPosition1 = groupPosition;
                childPosition1 = childPosition;

                Receipt receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1);

                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("title", receipt.getTitle());
                startActivity(intent);

                return false;
            }
        });

        for(int i=0; i < expandableListAdapter.getGroupCount(); i++)
            expandableListView.expandGroup(i);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (Constant.ButtonSnackBar.IS_SUBMIT_SUCCESS) {

            if (groupPosition1 == 0) { //1. First group click
                if (childPosition1 == 0) { //first click
                    Constant.ButtonSnackBar.IS_BACK_AVAILABLE = false;
                    Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = true;

                    back_receipt = null;

                    if(expandableListAdapter.getChildrenCount(groupPosition1) ==1) {

                        if (expandableListAdapter.getGroupCount()>1) {
                            next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1 + 1, 0);

                            Toast.makeText(MainActivity.this, "next : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                        } else {
                            next_receipt = null;
                            Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = false;
                        }

                    } else {
                        next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 + 1);

                        Toast.makeText(MainActivity.this, "next : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                    }


                } else if(childPosition1 == expandableListAdapter.getChildrenCount(groupPosition1) - 1) { //last click
                    Constant.ButtonSnackBar.IS_BACK_AVAILABLE = true;
                    Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = true;

                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 - 1);

                    if (expandableListAdapter.getGroupCount()>1) {
                        next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1 +1, 0);
                        Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle() + "\nnext : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                    } else {
                        next_receipt = null;
                        Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = false;
                    }

                } else { //other click
                    Constant.ButtonSnackBar.IS_BACK_AVAILABLE = true;
                    Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = true;

                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 - 1);
                    next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 + 1);
                    Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle() + "\nnext : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                }
            } else if (groupPosition1 == expandableListAdapter.getGroupCount() - 1) { //2. last group click
                if(childPosition1 == 0) { //first click
                    Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = true;
                    Constant.ButtonSnackBar.IS_BACK_AVAILABLE = true;

                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1 -1, expandableListAdapter.getChildrenCount(groupPosition1-1) -1);

                    if(expandableListAdapter.getChildrenCount(groupPosition1) == 1) {
                        Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = false;

                        Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                    } else {
                        next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 + 1);

                        Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle() + "\nnext : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                    }


                } else if (childPosition1 == expandableListAdapter.getChildrenCount(groupPosition1) - 1){ // last click
                    Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = false;
                    Constant.ButtonSnackBar.IS_BACK_AVAILABLE = true;

                    next_receipt = null;
                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 - 1);

                    Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                } else {//other click
                    Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = true;
                    Constant.ButtonSnackBar.IS_BACK_AVAILABLE = true;

                    next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 + 1);
                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 - 1);

                    Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle() + "\nnext : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                }
            } else { // 3. other group click
                Constant.ButtonSnackBar.IS_BACK_AVAILABLE = true;
                Constant.ButtonSnackBar.IS_NEXT_AVAILABLE = true;

                if (childPosition1 == 0) { //first click
                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1 -1, expandableListAdapter.getChildrenCount(groupPosition1-1)-1);

                    if (expandableListAdapter.getChildrenCount(groupPosition1) == 1) {
                        next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1+1, 0);
                    } else {
                        next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 + 1);
                    }

                    Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle() + "\nnext : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                } else if (childPosition1 == expandableListAdapter.getChildrenCount(groupPosition1) -1) {//last click
                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1-1);
                    next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1+1, 0);

                    Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle() + "\nnext : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                } else { //other click
                    next_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 + 1);
                    back_receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1 - 1);

                    Toast.makeText(MainActivity.this, "back : " + back_receipt.getTitle() + "\nnext : " + next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                }
            }



            Receipt receipt = (Receipt) expandableListAdapter.getChild(groupPosition1, childPosition1);

            if (Constant.ButtonSnackBar.IS_BACK_CLICKED) {
                SnackBar.show(MainActivity.this, llMain, back_receipt.getTitle() + " was submitted");
            } else if (Constant.ButtonSnackBar.IS_NEXT_CLICKED) {
                SnackBar.show(MainActivity.this, llMain, next_receipt.getTitle() + " was submitted");
            } else {
                SnackBar.show(MainActivity.this, llMain, receipt.getTitle() + " was submitted");
            }

            Constant.ButtonSnackBar.IS_SUBMIT_SUCCESS = false;
            Constant.ButtonSnackBar.IS_BACK_CLICKED = false;
            Constant.ButtonSnackBar.IS_NEXT_CLICKED = false;
        }

    }
}
