package com.bunna.snackbardemoz;

/**
 * Created by ChoeungBunna on 11/28/2017.
 */

public class Receipt {

    private String title;
    private String price;
    private boolean is_submitted;

    public Receipt() {
        this.title = "default";
        this.price = "000000";
        this.is_submitted = true;
    }

    public Receipt(String title, String price, boolean is_submitted) {
        this.title = title;
        this.price = price;
        this.is_submitted = is_submitted;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isIs_submitted() {
        return is_submitted;
    }

    public void setIs_submit(boolean is_submit) {
        this.is_submitted = is_submit;
    }
}
