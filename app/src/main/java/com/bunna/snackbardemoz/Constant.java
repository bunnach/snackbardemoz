package com.bunna.snackbardemoz;

/**
 * Created by ChoeungBunna on 11/28/2017.
 */

public class Constant {

    public static class ButtonSnackBar {
        public static boolean IS_BACK_AVAILABLE = false;

        public static boolean IS_NEXT_AVAILABLE = false;

        public static boolean IS_SUBMIT_SUCCESS = false;

        public static boolean IS_BACK_CLICKED = false;

        public static boolean IS_NEXT_CLICKED = false;
    }

}
