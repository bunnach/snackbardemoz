package com.bunna.snackbardemoz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<Receipt>> getData() {
        HashMap<String, List<Receipt>> expandableListDetail = new HashMap<>();

        List<Receipt> first = new ArrayList<>();
        first.add(new Receipt("Receipt 11", "111111", true));
        first.add(new Receipt("Receipt 22", "222222", true));
        first.add(new Receipt("Receipt 33", "333333", true));
        first.add(new Receipt("Receipt 44", "444444", true));
        first.add(new Receipt("Receipt 55", "555555", true));

        List<Receipt> second = new ArrayList<>();
        second.add(new Receipt("Receipt 66", "666666", true));
//        second.add(new Receipt("Receipt 77", "777777", true));
//        second.add(new Receipt("Receipt 88", "8888888", true));
//        second.add(new Receipt("Receipt 99", "999999", true));
//        second.add(new Receipt("Receipt 10", "10101010", true));

        List<Receipt> third = new ArrayList<>();
//        third.add(new Receipt("Receipt 11", "1111111", true));
//        third.add(new Receipt("Receipt 12", "12121212", true));
        third.add(new Receipt("Receipt 13", "13131313", true));
//        third.add(new Receipt("Receipt 14", "14141414", true));
        third.add(new Receipt("Receipt 15", "15151515", true));

        expandableListDetail.put("First", first);
        expandableListDetail.put("Second", second);
//        expandableListDetail.put("Third", third);
        return expandableListDetail;
    }
}
