package com.bunna.snackbardemoz;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Huo Chhunleng on 14/07/2016.
 */



/*
*  Usage
*  -gradle
*   compile 'com.android.support:design:23.0.1'
*
*   Ex. LinearLayout coordinatorLayout = (LinearLayout) findViewById(R.id.a007_4);
        SnackBar.show(A007_4Activity.this, coordinatorLayout, getResources().getString(R.string.A007_1P_1));
*
*/

public class SnackBar {

    public static void show(final Context context, View containerLayout, String message){
        try{
            // Snackbar default duration is 3.5s
            final Snackbar snackbar = Snackbar.make(containerLayout, "", Snackbar.LENGTH_LONG);
            // Get the Snackbar's layout view

            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();

            /* Note default padding are (24, 0, 24, 0)
                Set default padding left and right to 0
            */
            layout.setPadding(0, 0, 0, 0);

            TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
            textView.setText(message);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
            textView.setTextColor(Color.rgb(255, 255, 255));

            TextView textAction = (TextView) layout.findViewById(android.support.design.R.id.snackbar_action);
            textAction.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
            textAction.setTextColor(Color.rgb(255, 255, 255));
            textAction.setPaintFlags(textAction.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

            float sizeInDP = 18.5f;
            int marginInDp = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, sizeInDP, context.getResources()
                            .getDisplayMetrics());

//            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            llp.setMargins(marginInDp, 0, marginInDp, 0); // llp.setMargins(left, top, right, bottom);*/
//            textView.setLayoutParams(llp);

            layout.setBackgroundColor(Color.rgb(47, 48, 48));

            snackbar.setAction("NEXT", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    snackbar.dismiss();
                    context.startActivity(new Intent(context, MoreActivity.class));

                }
            });

            // Show the Snackbar
            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
