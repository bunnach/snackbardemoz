package com.bunna.snackbardemoz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Admin-PC on 11/22/2017.
 */

public class MoreActivity extends Activity implements View.OnClickListener {

    private TextView btnPrevious, btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_activity);

        btnPrevious = (TextView) findViewById(R.id.tvA);
        btnNext = (TextView) findViewById(R.id.tvB);

        btnPrevious.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        findViewById(R.id.btnClose).setOnClickListener(this);
        findViewById(R.id.rl_main).setOnClickListener(this);

        if (!Constant.ButtonSnackBar.IS_BACK_AVAILABLE) {
            btnPrevious.setText("Previous Disable");
            btnPrevious.setEnabled(false);
        } else {
            btnPrevious.setText("Previous");
            btnPrevious.setEnabled(true);
        }

        if (!Constant.ButtonSnackBar.IS_NEXT_AVAILABLE) {
            btnNext.setText("Next Disable");
            btnNext.setEnabled(false);
        } else {
            btnNext.setText("Next");
            btnNext.setEnabled(true);
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                    finish();
                break;

            case R.id.tvA:
                    finish();
                    Constant.ButtonSnackBar.IS_BACK_CLICKED = true;
//                    Toast.makeText(this, MainActivity.back_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MoreActivity.this, DetailActivity.class);
                    intent.putExtra("title", MainActivity.back_receipt.getTitle());
                    startActivity(intent);
                break;

            case R.id.tvB:
                    finish();
                    Constant.ButtonSnackBar.IS_NEXT_CLICKED = true;
//                    Toast.makeText(this, MainActivity.next_receipt.getTitle(), Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(MoreActivity.this, DetailActivity.class);
                intent1.putExtra("title", MainActivity.next_receipt.getTitle());
                startActivity(intent1);
                break;

            case R.id.rl_main:
                finish();
                break;
        }
    }
}
